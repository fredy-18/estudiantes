import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user/user.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  users:any;
  loading:boolean;
  constructor(private userService: UserService) { }
 
  ngOnInit(): void {
    this.get_students();
    
  }
  get_students(){
    this.userService.get_students()
    .subscribe((resp: any) => {
      this.users= resp.users
    },
    error => {});
  }
  delete_student(id){

    Swal.fire({
      title: "¿Esta seguro de eliminar el estudiante ?",
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Aceptar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.userService.delete_student(id)
        .subscribe((resp: any) => {
          if(resp.success)
            this.get_students();

          Swal.fire({
            icon: resp.success ? "info" : 'error',
            title:  resp.success ? "Exito" : 'Oops...',
            text: resp.message ,
            
          })
        });
      }
     
    });
   
    this.loading = false;
    
  }

}
