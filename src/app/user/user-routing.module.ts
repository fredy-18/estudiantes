import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'app/guard/admin.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: {
      title: 'Lista de estudiantes'
    },
    canActivate: [AdminGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: {
      title: 'Editar estudiante'
    },
    canActivate: [AdminGuard]
  },
  {
    path: 'new',
    component: FormComponent,
    data: {
      title: 'Crear estudiante'
    },
    canActivate: [AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
