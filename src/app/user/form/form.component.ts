import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/services/user/user.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  userForm:FormGroup ;
  constructor(private userService: UserService,private router: Router, private activatedRoute: ActivatedRoute) { }
  id:number = -1
  userData:any  = {
    name:"",
    email:"",
    phone: null,
    password: "",
    password_confirmation:""
  };
  erros:any =[];
  loading:boolean = false;
  success:string = null
  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;
    if(id != undefined && id != "") { 
      this.id = id;
      if(id > 0)
        this.get_user(id);
    }
    
    this.crea_form(this.userData)
  }
  get_user(id){
    
    this.loading = true;
    this.userService.get_user(this.id)
    .subscribe((resp: any) => {
      if(resp.success){
        let user =resp.user; 
        this.userData.name =user.name;
        this.userData.email =user.email;
        this.userData.phone =user.phone;
        this.userData.password  ="";
        this.userData.password_confirmation = "";
      }else{
        this.id = -1;
        Swal.fire({
          title: 'Usuario no encontrado',
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          denyButtonText: `Cancelar`,
        }).then((result) => {
          this.router.navigate(['/students']);
        });
      }
      this.loading = false;
    },
    error => {
      this.loading = false;
      // obnemeos los errores
      let erros = [];
      let e = error.error.errors;
      for (const key in e) {
        for (const key2 in e[key]) {
          erros.push(e[key][key2])
        }
        
      }
      console.log(erros);
      this.erros = erros;
      this.clear_error();
    });
  }
 
  get lf() {
    return this.userForm.controls;
  }
  crea_form(user:any={}){
   let group = {
    name: new FormControl(user.name, [Validators.required]),
    email: new FormControl(user.email, [Validators.email, Validators.required]),
    phone: new FormControl(user.phone),
    password: new FormControl(user.password),
    password_confirmation: new FormControl(user.password_confirmation)
    };
    if(this.id <= 0){
      group["password"] = new FormControl(user.password, [Validators.required]);
      group["password_confirmation"] = new FormControl(user.password_confirmation, [Validators.required]);
    }
    this.userForm = new FormGroup(group);
  }
  is_new(){
    return this.id <= 0;
  }
  save_user(form: FormGroup){
    this.loading = true;
    this.userService.save_user(this.userData, this.id)
    .subscribe((resp: any) => {
      if(resp.success){
        Swal.fire('Registro exitoso...', "Exito", 'success');
        this.userForm.reset();
        if(this.id > 0){
          this.router.navigate(['/students']);
        }
        this.id = -1;
      }else{
        
        Swal.fire(resp.message, "error", 'error');
      }
      this.loading = false;
    },
    error => {
      this.loading = false;
      // obnemeos los errores
      let erros = [];
      let e = error.error.errors;
      for (const key in e) {
        for (const key2 in e[key]) {
          erros.push(e[key][key2])
        }
        
      }
      console.log(erros);
      this.erros = erros;
      this.clear_error();
    });
  }
  is_valid_password(){    
    return this.userData.password == this.userData.password_confirmation
  }
  clear_error(){
    setTimeout(()=>{
      this.erros = [];
    },5000);
  }

}
