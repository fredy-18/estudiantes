import { Component, OnInit } from '@angular/core';
import { CoursesService } from 'app/services/courses/courses.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  courses: any = [];
  loading: boolean = false;
  constructor(private courseService: CoursesService) { }

  ngOnInit(): void {
    this.get_courses();
  }
  
  get_courses(){
    this.courseService.get_courses()
    .subscribe((resp: any) => {
      this.courses= resp.courses
    },
    error => {});
  }
  delete_student(id){

    Swal.fire({
      title: "¿Esta seguro de eliminar el curso ?",
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Aceptar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.courseService.delete_course(id)
        .subscribe((resp: any) => {
          if(resp.success)
            this.get_courses();

          Swal.fire({
            icon: resp.success ? "info" : 'error',
            title:  resp.success ? "Exito" : 'Oops...',
            text: resp.message ,
            
          })
        });
      }
     
    });
   
    this.loading = false;
    
  }
}
