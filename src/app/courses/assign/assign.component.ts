import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesService } from 'app/services/courses/courses.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent implements OnInit {
  loading:boolean = false
  course_id:number =-1;
  erros:any = [];
  course:any ={};
  course_students:any = [];
  courseForm:FormGroup;
  user_id:number =  -1;
  students = [];
  keyword:string = 'name';

  constructor(private router: Router,private courseService: CoursesService,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let course_id = this.activatedRoute.snapshot.params.course_id;
    if(course_id != undefined && course_id != "") { 
      this.course_id = course_id;
      if(course_id > 0){
        this.get_course(course_id);
        this.get_students_course(course_id)
      }
    }
  }
 
  selectEvent(item) {
    this.user_id = item.id;
  }

  onChangeSearch(search: string) {
    this.courseService.search_student(search).subscribe((resp: any) => {
      this.students = resp.students;      
    });
  }

  onFocused(e) {}
  clear(){
    this.user_id = -1;
  }
  
  delete_student(id){
    Swal.fire({
      title: "¿Esta seguro de eliminar el curso ?",
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Aceptar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.courseService.delete_course_student(id).subscribe((resp: any) => {
          if(resp.success)
          this.get_students_course(this.course_id);
          Swal.fire({
            icon: resp.success ? "info" : 'error',
            title:  resp.success ? "Exito" : 'Oops...',
            text: resp.message ,
            
          })
        });
      }
     
    });
  }
  get_students_course(course_id){
    this.courseService.get_students_course(course_id)
    .subscribe((resp: any) => {
      this.course_students= resp.students
    },
    error => {});
  }

  get_course(id){
    
    this.loading = true;
    this.courseService.get_course(id)
    .subscribe((resp: any) => {
      if(resp.success){
        this.course =resp.course; 
      }else{
        this.course_id = -1;
        Swal.fire({
          title: 'Curso no encontrado',
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          denyButtonText: `Cancelar`,
        }).then((result) => {
          this.router.navigate(['/courses']);
        });
      }
      this.loading = false;
    },
    error => {
      this.loading = false;
      // obnemeos los errores
      let erros = [];
      let e = error.error.errors;
      for (const key in e) {
        for (const key2 in e[key]) {
          erros.push(e[key][key2])
        }
        
      }
      console.log(erros);
    });
  }
  save_course_student(){
    this.loading = true;
    let data:any = {
      user_id: this.user_id,
      course_id: this.course_id
    }
    this.courseService.save_course_student(data)
    .subscribe((resp: any) => {
      if(resp.success){
        Swal.fire('Registro exitoso...', "Exito", 'success');
        this.user_id = -1;
        this.get_students_course(this.course_id);
      }else{
        
        Swal.fire(resp.message, "error", 'error');
      }
      this.loading = false;
    },
    error => {
      this.loading = false;
      // obnemeos los errores
      let erros = [];
      let e = error.error.errors;
      for (const key in e) {
        for (const key2 in e[key]) {
          erros.push(e[key][key2])
        }
        
      }
      this.erros = erros;
      this.clear_error();
    });
  }
  clear_error(){
    setTimeout(()=>{
      this.erros = [];
    },5000);
  }

}
