import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesRoutingModule } from './courses-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { AssignComponent } from './assign/assign.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoursesService } from 'app/services/courses/courses.service';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { MyCoursesComponent } from './my-courses/my-courses.component';

@NgModule({
  declarations: [
    FormComponent,
    ListComponent,
    AssignComponent,
    MyCoursesComponent
  ],
  imports: [
    CommonModule,
    CoursesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule
  ],
  providers: [CoursesService]
})
export class CoursesModule { }
