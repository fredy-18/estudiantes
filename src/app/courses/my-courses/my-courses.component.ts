import { Component, OnInit } from '@angular/core';
import { CoursesService } from 'app/services/courses/courses.service';

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.scss']
})
export class MyCoursesComponent implements OnInit {
  courses:any;
  constructor(private courseService: CoursesService) { }

  ngOnInit(): void {
    this.get_courses();
  }
  get_courses(){
    this.courseService.get_student_courses()
    .subscribe((resp: any) => {
      this.courses= resp.cousers
    },
    error => {});
  }
}
