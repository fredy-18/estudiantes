import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesService } from 'app/services/courses/courses.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  
  constructor(private courseService: CoursesService,private router: Router, private activatedRoute: ActivatedRoute) { }
  courseForm:FormGroup ;
  id:number = -1
  courseData:any  = {
    name:"",
    intensity:0
  };
  erros:any =[];
  loading:boolean = false;
  success:string = null
  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;
    if(id != undefined && id != "") { 
      this.id = id;
      if(id > 0)
        this.get_course(id);
    }
    
    this.crea_form(this.courseData)
  }
  get_course(id){
    
    this.loading = true;
    this.courseService.get_course(this.id)
    .subscribe((resp: any) => {
      if(resp.success){
        let course =resp.course; 
        this.courseData.name =course.name;
        this.courseData.intensity =course.intensity;
       
      }else{
        this.id = -1;
        Swal.fire({
          title: 'Usuario no encontrado',
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          denyButtonText: `Cancelar`,
        }).then((result) => {
          this.router.navigate(['/students']);
        });
      }
      this.loading = false;
    },
    error => {
      this.loading = false;
      // obnemeos los errores
      let erros = [];
      let e = error.error.errors;
      for (const key in e) {
        for (const key2 in e[key]) {
          erros.push(e[key][key2])
        }
        
      }
      console.log(erros);
      this.erros = erros;
      this.clear_error();
    });
  }
 
  get lf() {
    return this.courseForm.controls;
  }
  crea_form(course:any={}){
   let group = {
    name: new FormControl(course.name, [Validators.required]),
    intensity: new FormControl(course.intensity, [Validators.required]),
    
    };
    
    this.courseForm = new FormGroup(group);
  }
  is_new(){
    return this.id <= 0;
  }
  save_course(form: FormGroup){
    this.loading = true;
    this.courseService.save_course(this.courseData, this.id)
    .subscribe((resp: any) => {
      if(resp.success){
        Swal.fire('Registro exitoso...', "Exito", 'success');
        this.courseForm.reset();
        if(this.id > 0){
          this.router.navigate(['/courses']);
        }
        this.id = -1;
      }else{
        
        Swal.fire(resp.message, "error", 'error');
      }
      this.loading = false;
    },
    error => {
      this.loading = false;
      // obnemeos los errores
      let erros = [];
      let e = error.error.errors;
      for (const key in e) {
        for (const key2 in e[key]) {
          erros.push(e[key][key2])
        }
        
      }
      console.log(erros);
      this.erros = erros;
      this.clear_error();
    });
  }
  
  clear_error(){
    setTimeout(()=>{
      this.erros = [];
    },5000);
  }

}

