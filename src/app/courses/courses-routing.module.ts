import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { AssignComponent } from './assign/assign.component';
import { MyCoursesComponent } from './my-courses/my-courses.component';
import { AdminGuard } from 'app/guard/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: {
      title: 'Lista de estudiantes'
    },
    canActivate: [AdminGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: {
      title: 'Editar cursos'
    },
    canActivate: [AdminGuard]
  },
  {
    path: 'new',
    component: FormComponent,
    data: {
      title: 'Crear curo'
    },
    canActivate: [AdminGuard]
  },
  {
    path: 'assign/:course_id',
    component: AssignComponent,
    data: {
      title: 'Asingnar curos'
    },
    canActivate: [AdminGuard]
  },
  {
    path: 'my-courses',
    component: MyCoursesComponent,
    data: {
      title: 'Asingnar curos'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
