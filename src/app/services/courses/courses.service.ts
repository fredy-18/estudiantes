import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private httpClient:HttpClient) { }

  get_courses()
  {   
    return this.httpClient.get<any>(`${environment.apiUrlBase + "api/courses/get_courses"}`,{});
  }
  save_course(data:any, id:number)
  {   
    return this.httpClient.post<any>(`${environment.apiUrlBase + "api/courses/save_course/"+id}`,data);
  }
  get_course(id:number)
  {   
    return this.httpClient.get<any>(`${environment.apiUrlBase + "api/courses/get_course/"+id}`,{});
  }
  delete_course(id:number)
  {   
    return this.httpClient.delete<any>(`${environment.apiUrlBase + "api/courses/delete_course/"+id}`,{});
  }
  get_students_course(course_id:number){
    return this.httpClient.get<any>(`${environment.apiUrlBase + "api/courses/get_students_course/"+course_id}`,{});

  }
  save_course_student(data:any)
  {   
    return this.httpClient.post<any>(`${environment.apiUrlBase + "api/courses/save_course_student"}`,data);
  }
  delete_course_student(id:any)
  {   
    return this.httpClient.delete<any>(`${environment.apiUrlBase + "api/courses/delete_course_student/"+id}`,{});
  }
  search_student(search:string){
    let params = new HttpParams({fromObject : {search:search}});   
    return this.httpClient.get<any>(`${environment.apiUrlBase+ "api/courses/search_student"}`,{ params: params });
  }

  get_student_courses(){
    return this.httpClient.get<any>(`${environment.apiUrlBase + "api/courses/get_student_courses"}`,{});

  }

}
