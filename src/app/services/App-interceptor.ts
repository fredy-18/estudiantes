import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpHeaders,
    
} from "@angular/common/http";
import { Observable } from 'rxjs'

//declare var toastr: any;

@Injectable()
export class AppInterceptor implements HttpInterceptor {
   
    constructor() { }
 
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let url = request.url;
       
        let token = localStorage.getItem("token");
        request = request.clone({
            headers: new HttpHeaders({ 'Authorization':"Bearer "+ token}),
            url:url            
        }); 
        
        return next.handle(request);
    }
}