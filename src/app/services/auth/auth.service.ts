import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { retry } from 'rxjs/operators';

@Injectable()
export class AuthService {
  

  constructor(public _firebaseAuth: AngularFireAuth, public router: Router,private httpClient: HttpClient) {
    
  }

 
  login(data)
  {   
    return this.httpClient.post<any>(`${environment.apiUrlBase + "api/auth/login"}`,data).pipe(retry(1)); 
  }
 
  me()
  {   
    return this.httpClient.post<any>(`${environment.apiUrlBase + "api/auth/me"}`,{}).pipe(retry(1)); 
  }
  
  logout() {
    return  this.httpClient.post<any>(`${environment.apiUrlBase + "api/auth/logout"}`,{}).pipe(retry(1));
  }
  get_current_user()
  {   
    let user = localStorage.getItem("user");
    if(user != null && user != "")
      return JSON.parse(user);
    return {};
  }
  isAuthenticated() {
    let token = localStorage.getItem("token");
    return token != null && token != "";
  }
}
