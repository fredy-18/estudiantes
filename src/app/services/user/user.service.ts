import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient:HttpClient) { }

  get_students()
  {   
    return this.httpClient.get<any>(`${environment.apiUrlBase + "api/users/get_students"}`,{});
  }
  save_user(data:any, id:number)
  {   
    return this.httpClient.post<any>(`${environment.apiUrlBase + "api/users/save_user/"+id}`,data);
  }
  get_user(id:number)
  {   
    return this.httpClient.get<any>(`${environment.apiUrlBase + "api/users/get_user/"+id}`,{});
  }
  delete_student(id:number)
  {   
    return this.httpClient.delete<any>(`${environment.apiUrlBase + "api/users/delete_user/"+id}`,{});
  }
  get_rol():string {
    let user:any = localStorage.getItem("user");
    let rol:string ="";
    if(user != null && user != ""){
      user = JSON.parse(user);
      rol = user.rol;
    }
    return rol;
  }

  
  
}
