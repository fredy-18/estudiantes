import { Component, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from 'app/services/auth/auth.service';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

  loginFormSubmitted = false;
  isLoginFailed:boolean = false;
  message:string ="";
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required,Validators.email]),
    password: new FormControl('', [Validators.required]),
    rememberMe: new FormControl(true)
  });


  constructor(private router: Router, private authService: AuthService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute) {
      let isAuth = this.authService.isAuthenticated();
   
      if (isAuth) {
        let user = JSON.parse(localStorage.getItem("user"));
        if(user.rol ="admin")
          this.router.navigate(['/students']);
        else this.router.navigate(['/courses/my-courses']);
      }
  }

  get lf() {
    return this.loginForm.controls;
  }

  // On submit button click
  onSubmit() {
    this.loginFormSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    let data = {
      email:this.loginForm.value.username,
      password:this.loginForm.value.password,
      device_name:"web"
    }
    this.spinner.show(undefined,
      {
        type: 'ball-triangle-path',
        size: 'medium',
        bdColor: 'rgba(0, 0, 0, 0.8)',
        color: '#fff',
        fullScreen: true
      });

    this.authService.login(data)
    .subscribe((resp: any) => {
      if(resp.success){
        localStorage.setItem("token",resp.token);
        localStorage.setItem("user",JSON.stringify(resp.user))
       
        if(resp.user.rol ="admin")
          this.router.navigate(['/students']);
        else this.router.navigate(['/courses/my-courses']);
      }else{
        this.isLoginFailed = true;
        this.message = resp.message;        
        this.spinner.hide(); 
        this.showErroes();      
      }
    },
    error => { 
      this.showErroes(); 
      this.isLoginFailed = true;
      if(error.status == 422){        
        this.message =" Datos de acceso incorrectos";
      }else  this.message = error.error.message;
        this.spinner.hide();
        console.log(error) 
      }
    );
    
  }
  showErroes(){
    Swal.fire('',
    this.message,
    'error')
  }

}
