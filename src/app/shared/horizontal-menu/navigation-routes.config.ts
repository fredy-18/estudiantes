import { RouteInfo } from '../vertical-menu/vertical-menu.metadata';

export const HROUTES: RouteInfo[] = [
  {
    path: '/students', title: 'Estudiantes', icon: 'ft-home', class: 'dropdown nav-item', isExternalLink: false, submenu: [], user:"admin"
  }
];
