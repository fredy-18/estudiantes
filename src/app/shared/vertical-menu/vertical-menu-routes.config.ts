import { RouteInfo } from './vertical-menu.metadata';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [
  {
    path: '/students', title: 'Estudiante', icon: 'ft-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [],user:"admin"
    
  },
  {
    path: '/courses', title: 'Curos', icon: 'ft-briefcase', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [],user:"admin"
    
  },
  {
    path: '/courses/my-courses', title: 'Mis cusros', icon: 'ft-briefcase', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [],user:"student"
    
  },
  
];
