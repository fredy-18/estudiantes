import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [
  {
    path: 'students',
    loadChildren: () => import('../../user/user.module').then(m => m.UserModule)
  },
  {
    path: 'courses',
    loadChildren: () => import('../../courses/courses.module').then(m => m.CoursesModule)
  }
];
